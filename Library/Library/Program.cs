﻿using LittleLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittleLibrary
{
    class Program
    {

        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Library library = new Library();
            char input;
            menu.ShowMenu();
            do
            {
                Console.WriteLine("Make your choice (Press 9 for show menu)");
                input = Convert.ToChar(Console.ReadLine());
                switch (input)
                {
                    case '1':
                        library.AddBook();  //
                        break;
                    case '2':
                        library.RemoveBook();   //
                        break;
                    case '3':
                        library.SortLibrary(true);  //
                        library.Print();
                        break;
                    case '4':
                        library.SortLibrary(false); //
                        library.Print();
                        break;
                    case '5':
                        library.Print();    //
                        break;
                    case '6':
                        library.PrintJournal(); 
                        break;
                    case '7':
                        menu.TakeBookMenu();      
                        library.TakeBook();
                        menu.Done();
                        break;
                    case '8':
                        library.SearchAuthor();
                        break;
                    case '9':
                        menu.ShowMenu();
                        break;
                    default:
                        Console.WriteLine("Incorrect input");
                        break;
                }
            } while (true);
        }
    }
}
